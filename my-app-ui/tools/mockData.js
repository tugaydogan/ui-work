const routes = [
  {
    id: 1,
    departureLocation: "İstanbul",
    arrivalLocation: "Ankara",
    distance: "453km",
  },
  {
    id: 2,
    departureLocation: "İstanbul",
    arrivalLocation: "Ordu",
    distance: "893km",
  },
  {
    id: 3,
    departureLocation: "İstanbul",
    arrivalLocation: "Sivas",
    distance: "892km",
  },
  {
    id: 4,
    departureLocation: "İstanbul",
    arrivalLocation: "İzmir",
    distance: "484km",
  },
  {
    id: 5,
    departureLocation: "Ankara",
    arrivalLocation: "Edirne",
    distance: "687km",
  },
];

const newRoute = {
  id: null,
  departureLocation: "",
  arrivalLocation: "",
  distance: "",
};

module.exports = {
  newRoute,
  routes,
};
