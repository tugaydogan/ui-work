import React, { useState, useEffect } from "react";
import RouteFormPage from "./CreateRoutePage";
import { getallRoutes, createRoute } from "../../redux/actions/routeActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

function ManageRouteFormPage({
  routes,
  getallRoutes,
  createRoute,
  history,
  ...props
}) {
  const [route, setRoute] = useState(...props.route);

  useEffect(() => {
    if (routes.length === 0) {
      getallRoutes().catch((error) => {
        alert("Loading error" + error);
      });
    } else {
      setRoute({ ...props.route });
    }
  }, [...props.route]);

  function handleChange(event) {
    const { name, value } = event.target;
    setRoute((prevRoute) => ({ ...prevRoute }));
  }

  function handleSave(event) {
    event.preventDefault();
    createRoute(route).then(() => {
      history.push("/route");
    });
  }
  return (
    <RouteFormPage route={route} onChange={handleChange} onSave={handleSave} />
  );
}

ManageRouteFormPage.propTypes = {
  route: PropTypes.object.isRequired,
  getallRoutes: PropTypes.func.isRequired,
  createRoute: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

function mapToStateToProps(state, ownProps) {
  return {
    route: state.route,
    routes: state.routes,
  };
}
const mapDispatchToProps = {
  getallRoutes,
  createRoute,
};

export default connect(
  mapToStateToProps,
  mapDispatchToProps
)(ManageRouteFormPage);
