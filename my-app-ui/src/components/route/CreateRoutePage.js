import React from "react";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import NavBar from "../common/NavBar";
import PropTypes from "prop-types";

const CreateRoutePage = ({ route, onSave, onChange, saving = false }) => {
  return (
    <div className="container">
      <Header />
      <NavBar />
      <div id="routeexpeditionsmargin">
        <h3>Rota Oluştur</h3>
      </div>
      <form onSubmit={onSave} id="createrouteformmargin">
        <div class="row">
          <div class="col">
            <label>Kalkış Yeri</label>
            <input
              name="tittle"
              type="text"
              value={route.kalkisyeri}
              onChange={onChange}
              class="form-control"
              placeholder="İstanbul"
            />
          </div>
          <div class="col">
            <label>Varış Yeri</label>
            <input
              name="tittle"
              type="text"
              value={route}
              onChange={onChange}
              class="form-control"
              placeholder="Ankara"
            />
          </div>
          <div class="col">
            <label>Mesafe</label>
            <input
              name="tittle"
              type="text"
              value={route}
              onChange={onChange}
              class="form-control"
              placeholder="450km"
            />
          </div>
        </div>
        <div id="routeexpeditionsmargin">
          <button type="submit" class="btn btn-success btn-lg">
            {saving ? "Saving" : "Save"}
          </button>
        </div>
      </form>
      <Footer />
    </div>
  );
};
CreateRoutePage.propTypes = {
  routes: PropTypes.array.isRequired,
  route: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
};

export default CreateRoutePage;
