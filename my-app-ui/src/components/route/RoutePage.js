import React from "react";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import { Link } from "react-router-dom";
import NavBar from "../common/NavBar";
import "../common/maincss.css";

function RoutePage() {
  return (
    <div className="container">
      <Header />
      <NavBar />
      <table id="routeexpeditionsmargin" class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Kalkış Yeri</th>
            <th scope="col">Varış Yeri</th>
            <th scope="col">Mesafe</th>
            <th scope="col">Mola Sayısı</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>İstanbul</td>
            <td>Ankara</td>
            <td>456 Km</td>
            <td></td>
            <td>
              <Link to="standard">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td></td>
            <td>
              <Link to="standard">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
            <td></td>
            <td>
              <Link to="standard">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
        </tbody>
      </table>
      <Footer />
    </div>
  );
}
export default RoutePage;
