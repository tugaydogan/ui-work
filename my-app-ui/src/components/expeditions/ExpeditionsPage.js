import React from "react";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import { Link } from "react-router-dom";
import NavBar from "../common/NavBar";
import "../common/maincss.css";

function ExpeditionsPage() {
  return (
    <div className="container">
      <Header />
      <NavBar />
      <p className="blog-header-logo text-dark">Sefer Listesi</p>
      <table id="routeexpeditionsmargin" class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Sefer Saati</th>
            <th scope="col">Kalkış Yeri-Varış Yeri</th>
            <th scope="col">Otobüs Tipi</th>
            <th scope="col">Fiyat</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>100₺</td>
            <td>
              <Link to="bus">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td></td>
            <td>
              <Link to="bus">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
            <td></td>
            <td>
              <Link to="bus">
                <button type="button" class="btn btn-outline-success">
                  Seç
                </button>
              </Link>
            </td>
          </tr>
        </tbody>
      </table>

      <Footer />
    </div>
  );
}
export default ExpeditionsPage;
