import React from "react";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import Navbar from "../common/NavBar";
import "./CreateExpeditionPage.css";

function ExpeditionPage() {
  return (
    <div className="container">
      <Header />
      <Navbar />
      <p className="blog-header-logo text-dark">Sefer Oluştur</p>
      <div id="createexpeditionpageleftblock">
        <form>
          <div class="form-group">
            <label for="exampleFormControlSelect2">Rota Seçiniz</label>
            <select
              multiple
              class="form-control"
              id="exampleFormControlSelect2"
            >
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect2">Otobüs Seçiniz</label>
            <select
              multiple
              class="form-control"
              id="exampleFormControlSelect2"
            >
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect2">Şoför Seçiniz</label>
            <select
              multiple
              class="form-control"
              id="exampleFormControlSelect2"
            >
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleFormControlSelect2">Muavin Seçiniz</label>
            <select
              multiple
              class="form-control"
              id="exampleFormControlSelect2"
            >
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
        </form>
      </div>
      <div id="createexpeditionrigthblock">
        <form>
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="defaultCheck1"
            />
            <label class="form-check-label" for="defaultCheck1">
              Yimah var?
            </label>
          </div>
          <div id="selectarrivaltime" className="form-group">
            <label for="formGroupExampleInput">Kalkış Zamanı</label>
            <input
              type="datetime-local"
              class="form-control"
              id="formGroupExampleInput"
              placeholder="Example input"
            />
          </div>
          <div id="selectdeparturetime" className="form-group">
            <label for="formGroupExampleInput">Varış Zamanı</label>
            <input
              type="datetime-local"
              class="form-control"
              id="formGroupExampleInput"
              placeholder="Example input"
            />
          </div>
          <button id="savebutton" type="button" class="btn btn-success">
            Kaydet
          </button>
        </form>
      </div>

      <Footer />
    </div>
  );
}

export default ExpeditionPage;
