import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { fetchAllExpeditions } from "../../redux/actions/expeditionActions";
import { getallBus } from "../../redux/actions/busActions";
import PropTypes from "prop-types";
import CreateExpeditionPage from "./CreateExpeditionPage";

function ManageExpeditionPage({
  expeditions,
  buses,
  hosts,
  routes,
  drivers,
  fetchAllExpeditions,
  addExpedition,
  history,
  ...props
}) {
  const [expedition, setExpedition] = useState({ ...props.expedition });

  useEffect(() => {
    if (expeditions.length === 0) {
      fetchAllExpeditions().catch((error) => {
        alert("Loading courses failed" + error);
      });
    } else {
      setExpedition({ ...props.course });
    }

    if (buses.length === 0) {
      getallBus().catch((error) => {
        alert("Loading authors failed" + error);
      });
    }
  }, [props.expeditions]);

  function handleAdd(event) {
    event.preventDefault();
    addExpedition(expedition).then(() => {
      history.push("/expeditions");
    });
  }
  function handleChangeBus(event) {
    const { name, value } = event.target;
    setExpedition((prevExpedition) => ({
      ...prevExpedition,
      [name]: name === "busId" ? parseInt(value, 10) : value,
    }));
  }
  return (
    <CreateExpeditionPage
      expedition={expedition}
      buses={buses}
      onChange={handleChangeBus}
      onAdd={handleAdd}
    />
  );
}
ManageExpeditionPage.propTypes = {
  expedition: PropTypes.object.isRequired,
  buses: PropTypes.array.isRequired,
  expeditipns: PropTypes.array.isRequired,
  fetchAllExpeditions: PropTypes.func.isRequired,
  getallDriver: PropTypes.func.isRequired,
  addExpedition: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    expeditions: state.expeditions,
    buses: state.buses,
  };
}

const mapDispatchToProps = {
  fetchAllExpeditions,
  getallBus,
};
