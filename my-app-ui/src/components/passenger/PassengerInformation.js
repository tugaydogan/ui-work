import React from "react";

function PassengerInfo() {
  return (
    <div className="container">
      <p className="blog-header-logo text-dark">Yolcu Bilgilerinizi Giriniz</p>
      <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="input">İsim</label>
            <input type="text" class="form-control" id="input" />
          </div>
          <div class="form-group col-md-6">
            <label for="input">Soyisim</label>
            <input type="text" class="form-control" id="input" />
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Kimlik Numarası</label>
          <input
            type="text"
            class="form-control"
            placeholder="12345678965"
            onkeypress="return event.charCode>= 48 &&event.charCode<= 57"
            maxLength="11"
          />
        </div>
        <div class="form-group">
          <label for="input">Doğum Tarihi</label>
          <input type="date" class="form-control" />
        </div>
        <label id="mahmut">Cinsiyet</label>
        <div class="form-check">
          <input
            class="form-check-input"
            type="radio"
            name="exampleRadios"
            id="exampleRadios1"
            value="option1"
            checked
          />
          <label class="form-check-label" for="exampleRadios1">
            Kadın
          </label>
        </div>
        <div class="form-check">
          <input
            class="form-check-input"
            type="radio"
            name="exampleRadios"
            id="exampleRadios2"
            value="option2"
          />
          <label class="form-check-label" for="exampleRadios2">
            Erkek
          </label>
        </div>
        <button
          id="kaydetmebuttonu"
          type="button"
          class="btn btn-success btn-lg"
        >
          Kaydet
        </button>
      </form>
    </div>
  );
}
export default PassengerInfo;
