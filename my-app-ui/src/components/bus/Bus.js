import React from "react";
import "./Bus.css";
import NavBar from "../common/NavBar";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import PassengerInfo from "../passenger/PassengerInformation";

class Bus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      /*seats: [
        { id: 1, seatnumber: 1 },
        { id: 2, seatnumber: 2 },
        { id: 3, seatnumber: 3 },
        { id: 4, seatnumber: 4 },
        { id: 5, seatnumber: 5 },
        { id: 6, seatnumber: 6 },
        { id: 7, seatnumber: 7 },
        { id: 8, seatnumber: 8 },
        { id: 9, seatnumber: 9 },
        { id: 10, seatnumber: 10 },
        { id: 11, seatnumber: 11 },
        { id: 12, seatnumber: 12 },
        { id: 13, seatnumber: 13 },
        { id: 14, seatnumber: 14 },
        { id: 15, seatnumber: 15 },
        { id: 16, seatnumber: 16 },
        { id: 17, seatnumber: 17 },
        { id: 18, seatnumber: 18 },
        { id: 19, seatnumber: 19 },
        { id: 20, seatnumber: 20 },
        { id: 21, seatnumber: 21 },
      ],
      */
      leftseats: [
        { id: 1, seatnumber: 1 },
        { id: 4, seatnumber: 4 },
        { id: 7, seatnumber: 7 },
        { id: 10, seatnumber: 10 },
        { id: 13, seatnumber: 13 },
        { id: 16, seatnumber: 16 },
        { id: 19, seatnumber: 19 },
      ],
      middleseats: [
        { id: 2, seatnumber: 2 },
        { id: 5, seatnumber: 5 },
        { id: 8, seatnumber: 8 },
        { id: 11, seatnumber: 11 },
        { id: 14, seatnumber: 14 },
        { id: 17, seatnumber: 17 },
        { id: 20, seatnumber: 20 },
      ],
      rightseats: [
        { id: 3, seatnumber: 3 },
        { id: 6, seatnumber: 6 },
        { id: 9, seatnumber: 9 },
        { id: 12, seatnumber: 12 },
        { id: 15, seatnumber: 15 },
        { id: 18, seatnumber: 18 },
        { id: 21, seatnumber: 21 },
      ],
      seatReserved: [],
    };
  }
  renderleftseats() {
    return this.state.leftseats.map((seat) => {
      const { seatnumber } = seat;
      return (
        <tr>
          <td>{seatnumber}</td>
        </tr>
      );
    });
  }
  renderrightseats() {
    return this.state.rightseats.map((seat) => {
      const { seatnumber } = seat;
      return (
        <tr>
          <td>{seatnumber}</td>
        </tr>
      );
    });
  }
  rendermiddleseats() {
    return this.state.middleseats.map((seat) => {
      const { seatnumber } = seat;
      return (
        <tr>
          <td key={seatnumber} onClick={(e) => this.onClickSeat(seatnumber)}>
            {seatnumber}
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="container">
        <Header />
        <NavBar />
        <h2>Koltuk Seçimi ve Yolcu Bilgileri</h2>
        <div id="busdiv">
          <table id="leftseatsstyle" className="grid">
            <tbody>{this.renderleftseats()}</tbody>
          </table>
          <table id="middleseatsstyle" className="grid">
            <tbody>{this.rendermiddleseats()}</tbody>
          </table>
          <table id="rightseatsstyle" className="grid">
            <tbody>{this.renderrightseats()}</tbody>
          </table>
        </div>
        <div id="passengerinfodiv">
          <PassengerInfo />
        </div>
        <Footer />
      </div>
    );
  }
}
export default Bus;
