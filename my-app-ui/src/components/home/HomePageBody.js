import React from "react";
import "./HomePageBody.css";
import GetExpeditionsButton from "../common/GetExpeditionsButton";
import { Link } from "react-router-dom";
import HomeSlider from "./HomeSlider";
import Calendar from "../calendar/Calendar";
import "./HomePageBody.css";

function HomePageBody() {
  return (
    <div>
      <div id="homebodyleftblock">
        <div className="homeslider">
          <HomeSlider />
        </div>
      </div>
      <div id="homebodyrightblock">
        <div className="expeditioncalendar">
          <label>Sefer Tarihi Seçiniz</label>
          <br />
          <Calendar />
        </div>
        <div className="getexpeditionbutton">
          <Link to="expeditions">
            <GetExpeditionsButton />
          </Link>
        </div>
      </div>
    </div>
  );
}
export default HomePageBody;
