import React from "react";
import { Link } from "react-router-dom";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import NavBar from "../common/NavBar";

function Host() {
  return (
    <div className="container">
      <Header />
      <NavBar />
      <p className="blog-header-logo text-dark">Muavin Listesi</p>
      <table id="routeexpeditionsmargin" class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Adı-Soyadı</th>
            <th scope="col">Kimlik Numarası</th>
            <th scope="col">Doğum Tarihi</th>
            <th scope="col">Cinsiyeti</th>
            <th scope="col"></th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>Karı</td>
            <td>
              <Link to="createhost">
                <button type="button" class="btn btn-outline-info">
                  Değiştir
                </button>
              </Link>
            </td>
            <td>
              <Link to="">
                <button type="button" class="btn btn-outline-danger">
                  Sil
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td></td>
            <td>
              <Link to="createhost">
                <button type="button" class="btn btn-outline-info">
                  Değiştir
                </button>
              </Link>
            </td>
            <td>
              <Link to="">
                <button type="button" class="btn btn-outline-danger">
                  Sil
                </button>
              </Link>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
            <td></td>
            <td>
              <Link to="createhost">
                <button type="button" class="btn btn-outline-info">
                  Değiştir
                </button>
              </Link>
            </td>
            <td>
              <Link to="">
                <button type="button" class="btn btn-outline-danger">
                  Sil
                </button>
              </Link>
            </td>
          </tr>
        </tbody>
      </table>
      <Footer />
    </div>
  );
}

export default Host;
