import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  hosts: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_HOST:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_HOST:
      return state.map((host) =>
        host.id === action.payload.id ? action.payload : host
      );
    case types.REQUEST_GET_BY_ID_HOST:
      return state.filter((host) => host.id === action.payload.id);
    case types.REQUEST_GET_ALL_HOST:
      return action.payload;
    case types.REQUEST_DELETE_HOST:
      return state.filter((host) => host.id !== action.payload.id);
    default:
      return state;
  }
}
