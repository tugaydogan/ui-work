import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  drivers: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_DRIVER:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_DRIVER:
      return state.map((driver) =>
        driver.id === action.payload.id ? action.payload : driver
      );
    case types.REQUEST_GET_BY_ID_DRIVER:
      return state.filter((driver) => driver.id === action.payload.id);
    case types.REQUEST_GET_ALL_DRIVER:
      return action.payload;
    case types.REQUEST_DELETE_DRIVER:
      return state.filter((driver) => driver.id !== action.payload.id);
    default:
      return state;
  }
}
