import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  expeditions: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_EXPEDITION:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_EXPEDITION:
      return state.map((expedition) =>
        expedition.id === action.payload.id ? action.payload : expedition
      );
    case types.REQUEST_GET_BY_ID_EXPEDITION:
      return state.filter((expedition) => expedition.id === action.payload.id);
    case types.REQUEST_GET_ALL_EXPEDITIONS:
      return action.payload;
    case types.REQUEST_DELETE_EXPEDITION:
      return state.filter((expedition) => expedition.id !== action.payload.id);
    default:
      return state;
  }
}
