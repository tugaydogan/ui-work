import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  routes: [],
  kalkisyeri: "",
  varisyeri: "",
  mesafe: 0,
  route: null,
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_ROUTE:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_ROUTE:
      return state.map((route) =>
        route.id === action.payload.id ? action.payload : route
      );
    case types.REQUEST_GET_BY_ID_ROUTE:
      return state.filter((route) => route.id === action.payload.id);
    case types.REQUEST_GET_ALL_ROUTE:
      return action.payload;
    case types.REQUEST_DELETE_ROUTE:
      return state.filter((route) => route.id !== action.payload.id);
    default:
      return state;
  }
}
