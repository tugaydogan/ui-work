import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  buses: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_BUS:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_BUS:
      return state.map((bus) =>
        bus.id === action.payload.id ? action.payload : bus
      );
    case types.REQUEST_GET_BY_ID_BUS:
      return state.filter((bus) => bus.id === action.payload.id);
    case types.REQUEST_GET_ALL_BUS:
      return action.payload;
    case types.REQUEST_DELETE_BUS:
      return state.filter((bus) => bus.id !== action.payload.id);
    default:
      return state;
  }
}
