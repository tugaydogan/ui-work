import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  tickets: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_CANCEL_TICKET:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_TICKET:
      return state.map((ticket) =>
        ticket.id === action.payload.id ? action.payload : ticket
      );
    case types.REQUEST_GET_BY_ID_TICKET:
      return state.filter((ticket) => ticket.id === action.payload.id);
    case types.REQUEST_GET_ALL_TICKET:
      return action.payload;
    case types.REQUEST_SELL_TICKET:
      return state.filter((ticket) => ticket.id !== action.payload.id);
    default:
      return state;
  }
}
