import * as types from "../actions/actionTypes";

const INITIAL_STATE = {
  passengers: [],
};

export default function expeditionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.REQUEST_ADD_NEW_PASSENGER:
      return [...state, { ...action.payload }];
    case types.REQUEST_UPDATE_PASSENGER:
      return state.map((passenger) =>
        passenger.id === action.payload.id ? action.payload : passenger
      );
    case types.REQUEST_GET_BY_ID_PASSENGER:
      return state.filter((passenger) => passenger.id === action.payload.id);
    case types.REQUEST_GET_ALL_PASSENGER:
      return action.payload;
    case types.REQUEST_DELETE_PASSENGER:
      return state.filter((passenger) => passenger.id !== action.payload.id);
    default:
      return state;
  }
}
