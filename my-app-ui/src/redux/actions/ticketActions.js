import * as types from "./actionTypes";
import Axios from "axios";
import baseurl from "../api/ticketApi";

export function sellTicket(ticketId) {
  return function (dispatch) {
    return Axios.delete(baseurl + ticketId).then((response) => {
      dispatch({ type: types.REQUEST_SELL_TICKET, payload: response.id });
    });
  };
}

export function cancelTicket(ticket) {
  return function (dispatch) {
    return Axios.post(baseurl + ticket).then((response) => {
      dispatch({ type: types.REQUEST_CANCEL_TICKET, payload: response.data });
    });
  };
}

export function updateTicket(ticket) {
  return function (dispatch) {
    return Axios.put(baseurl + ticket).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_TICKET, payload: response });
    });
  };
}
export function getByIdTicket(ticketId) {
  return function (dispatch) {
    return Axios.get(baseurl + ticketId).then((response) => {
      dispatch({
        type: types.REQUEST_GET_BY_ID_TICKET,
        payload: response.data,
      });
    });
  };
}

export function getallTickets() {
  return function (dispatch) {
    return Axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_TICKET, payload: response });
    });
  };
}
