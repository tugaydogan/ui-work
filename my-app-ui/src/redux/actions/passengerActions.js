import * as types from "./actionTypes";
import baseurl from "../api/passengerApi";
import Axios from "axios";

export function createPassenger(passenger) {
  return function (dispatch) {
    return Axios.post(baseurl + passenger).then((response) => {
      dispatch({ type: types.REQUEST_ADD_NEW_ROUTE, payload: response.data });
    });
  };
}

export function deletePassenger(passengerId) {
  return function (dispatch) {
    return Axios.delete(baseurl + passengerId).then((response) => {
      dispatch({ type: types.REQUEST_DELETE_ROUTE, payload: passengerId });
    });
  };
}

export function updatePassenger(passenger) {
  return function (dispatch) {
    return Axios.put(baseurl + passenger).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_ROUTE, payload: passenger });
    });
  };
}
export function getByIdPassenger(passengerId) {
  return(dispatch){
    return Axios.get(baseurl+passengerId).then((response)=>{
      dispatch({type: types.REQUEST_GET_BY_ID_PASSENGER, payload:response})
    })
  }
}

export function getallPassenger() {
  return function(dispatch){
    return Axios.get(baseurl).then((response)=>{
      dispatch({ type: types.REQUEST_GET_ALL_PASSENGER, payload:response });
    })
  }
}
