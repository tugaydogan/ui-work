import * as types from "./actionTypes";
import baseurl from "../api/expeditionApi";
import Axios from "axios";

/*
export function createExpedition(expedition) {
  return { type: types.REQUEST_ADD_NEW_EXPEDITION, expedition };
}

export function deleteExpedition(expedition) {
  return { type: types.REQUEST_DELETE_EXPEDITION, expedition };
}

export function updateExpedition(expedition) {
  return { type: types.REQUEST_UPDATE_EXPEDITION, expedition };
}

export function getByIdExpedition(expedition) {
  return { type: types.REQUEST_GET_BY_ID_EXPEDITION, expedition };
}

export function getallExpeditions(expeditions) {
  return { type: types.REQUEST_GET_ALL_EXPEDITIONS, expeditions };
}
*/

export function fetchExpeditionById(expeditionId) {
  return function (dispatch) {
    return Axios.get(baseurl + expeditionId).then((response) => {
      dispatch({
        type: types.REQUEST_GET_BY_ID_EXPEDITION,
        payload: response.data,
      });
    });
  };
}

export function fetchAllExpeditions() {
  return function (dispatch) {
    return Axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_EXPEDITIONS, payload: response });
    });
  };
}

export function addExpedition(expedition) {
  return function (dispatch) {
    return Axios.post(baseurl + expedition).then((response) => {
      dispatch({
        type: types.REQUEST_ADD_NEW_EXPEDITION,
        payload: response.data,
      });
    });
  };
}
export function updateExpedition(expedition) {
  return function (dispatch) {
    return Axios.put(baseurl + expedition).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_EXPEDITION, payload: response });
    });
  };
}

export function deleteExpedition(expeditionId) {
  return function (dispatch) {
    return Axios.delete(baseurl + expeditionId).then((response) => {
      dispatch({
        type: types.REQUEST_DELETE_EXPEDITION,
        payload: response.id,
      });
    });
  };
}
