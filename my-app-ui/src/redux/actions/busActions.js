import * as types from "./actionTypes";
import Axios from "axios";
import baseurl from "../api/busApi";

export function createBus(bus) {
  return function (dispatch) {
    return Axios.post(baseurl + bus).then((response) => {
      dispatch({ type: types.REQUEST_ADD_NEW_DRIVER, payload: response });
    });
  };
}

export function deleteBus(busId) {
  return function (dispatch) {
    return Axios.delete(baseurl + busId).then((response) => {
      dispatch({ type: types.REQUEST_DELETE_BUS, payload: busId });
    });
  };
}

export function updateBus(bus) {
  return function (dispatch) {
    return Axios.put(baseurl + bus).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_BUS, payload: response });
    });
  };
}
export function getByIdBus(busId) {
  return function (dispatch) {
    return Axios.get(baseurl + busId).then((response) => {
      dispatch({ type: types.REQUEST_GET_BY_ID_BUS, payload: response.data });
    });
  };
}

export function getallBus() {
  return function (dispatch) {
    return Axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_BUS, payload: response });
    });
  };
}
