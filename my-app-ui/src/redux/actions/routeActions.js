import * as types from "./actionTypes";
import baseurl from "../api/routeApi";
import Axios from "axios";

export function createRoute(route) {
  return function (dispatch) {
    return Axios.post(baseurl + route).then((response) => {
      dispatch({ type: types.REQUEST_ADD_NEW_ROUTE, payload: response.data });
    });
  };
}

export function deleteRoute(routeId) {
  return function (dispatch) {
    return Axios.delete(baseurl + routeId).then((response) => {
      dispatch({ type: types.REQUEST_DELETE_ROUTE, payload: routeId });
    });
  };
}

export function updateRoute(route) {
  return function (dispatch) {
    return Axios.put(baseurl + route).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_ROUTE, payload: response });
    });
  };
}
export function getByIdRoute(routeId) {
  return function (dispatch) {
    return Axios.get(baseurl + routeId).then((response) => {
      dispatch({
        type: types.REQUEST_GET_BY_ID_ROUTE,
        payload: response.data,
      });
    });
  };
}

export function getallRoutes() {
  return function (dispatch) {
    return Axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_ROUTE, payload: response });
    });
  };
}
