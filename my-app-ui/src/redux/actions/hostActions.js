import * as types from "./actionTypes";
import baseurl from "../api/hostApi";
import Axios from "axios";

export function createHost(host) {
  return function (dispatch) {
    return Axios.post(baseurl + host).then((response) => {
      dispatch({ type: types.REQUEST_ADD_NEW_HOST, payload: response.data });
    });
  };
}

export function deleteHost(hostId) {
  return function (dispatch) {
    return Axios.delete(baseurl + hostId).then((response) => {
      dispatch({ type: types.REQUEST_DELETE_HOST, payload: hostId });
    });
  };
}

export function updateHost(host) {
  return function (dispatch) {
    return Axios.put(baseurl + host).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_HOST, payload: host });
    });
  };
}
export function getByIdHosT(hostId) {
  return function (dispatch) {
    return Axios.get(baseurl + hostId).then((response) => {
      dispatch({
        type: types.REQUEST_GET_BY_ID_HOST,
        payload: response.data,
      });
    });
  };
}

export function getallHost() {
  return function (dispatch) {
    return Axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_HOST, payload: response });
    });
  };
}
