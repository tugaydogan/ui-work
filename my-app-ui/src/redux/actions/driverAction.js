import * as types from "./actionTypes";
import axios from "axios";
import baseurl from "../api/driverApi";

export function createDriver(driver) {
  return function (dispatch) {
    return axios.post(baseurl + driver).then((response) => {
      dispatch({ type: types.REQUEST_ADD_NEW_DRIVER, payload: response });
    });
  };
}

export function deleteDriver(driverId) {
  return function (dispatch) {
    return axios.delete(baseurl + driverId).then((response) => {
      dispatch({ type: types.REQUEST_DELETE_DRIVER, payload: driverId });
    });
  };
}

export function updateDriver(driver) {
  return function (dispatch) {
    return axios.put(baseurl + driver).then((response) => {
      dispatch({ type: types.REQUEST_UPDATE_DRIVER, payload: response });
    });
  };
}
export function getByIdDriver(driverId) {
  return function (dispatch) {
    return axios.get(baseurl + driverId).then((response) => {
      dispatch({ type: types.REQUEST_GET_BY_ID_DRIVER, payload: response });
    });
  };
}

export function getallDriver() {
  return function (dispatch) {
    return axios.get(baseurl).then((response) => {
      dispatch({ type: types.REQUEST_GET_ALL_DRIVER, payload: response });
    });
  };
}
