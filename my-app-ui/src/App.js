import React from "react";
import "./App.css";
import HomePage from "../src/components/home/HomePage";
import { Switch, Route } from "react-router-dom";
import ExpeditionsPage from "../src/components/expeditions/ExpeditionsPage";
import PageNotFound from "../src/components/errors/PageNotFound";
import UserLoginPage from "./components/user/UserLoginPage";
import SignUpPage from "../src/components/user/SignUp";
import CreateRoute from "../src/components/route/CreateRoutePage";
import RoutePage from "../src/components/route/RoutePage";
import CreateExpeditionPage from "../src/components/expeditions/CreateExpeditionPage";
import CreateHost from "./components/worker/CreateHostPage";
import HostPage from "./components/worker/HostPage";
import CreateDriver from "./components/worker/CreateDriverPage";
import DriverPage from "./components/worker/DriverPage";
import BusPage from "../src/components/bus/Bus";

//<Route path="/hosts" component={HostPage} />
//<Route path="/drivers" component={DriverPage} />

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/expeditions" component={ExpeditionsPage} />
        <Route path="/home" component={HomePage} />
        <Route path="/login" component={UserLoginPage} />
        <Route path="/signup" component={SignUpPage} />
        <Route path="/createRoute" component={CreateRoute} />
        <Route path="/routes" component={RoutePage} />
        <Route path="/createExpedition" component={CreateExpeditionPage} />
        <Route path="/createhost" component={CreateHost} />
        <Route path="/createdriver" component={CreateDriver} />
        <Route path="/hosts" component={HostPage} />
        <Route path="/drivers" component={DriverPage} />
        <Route path="/bus" component={BusPage} />
        <Route component={PageNotFound}></Route>
      </Switch>
    </div>
  );
}

export default App;
